#!/bin/bash
CONTAINER_NAME=r6s-api-relay
IMAGE_VERSION=master
echo "Stopping old intance ..."
docker stop ${CONTAINER_NAME}

echo "Delete old instance ..."
docker rm ${CONTAINER_NAME}

echo "Starting new instance ..."
screen -AmdS ${CONTAINER_NAME}_docker \
	docker run -it \
        --rm \
        -p 7766:8080 \
	-v /opt/r6s-api-relay/credentials.json:/opt/app/credentials.json \
        --name=${CONTAINER_NAME} \
        registry.gitlab.com/siege-insights/r6s-api-relay:${IMAGE_VERSION}
