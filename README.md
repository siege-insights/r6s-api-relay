# Rainbow Six: Siege API Relay
## Description
The goal of this project is to provide a simple and relaiable relay-proxy to make requests against the UPlay API for Rainbow Six: Siege.
The main use is to fetch player stats from the UPlay backend in order to build something amazing using this data.

It usese a list of accounts (see `credentials.sample.json`) to login against the `UPlay API`.
For each request, another credential is used, once we've reached the last, we start from the beginning again (Round-Robin).

We utilize the existing framework from billy-yoyo [RainbowSixSiege-Python-API](https://github.com/billy-yoyo/RainbowSixSiege-Python-API)
and just adding the multi user management and REST-Server so you can use a different programming language for your implementation and don't have to stick to Python :snake:


## Why is this required?
UPlay requires a valid session in order to fetch details about player. Also they have pretty hard API limitations which is why we need to use multiple accounts to fetch a lot of data. Otherwise limiter would hit and block / deny requests.

## Security
Keep in mind that there is no protection at all. This was designed to run on a server with a restrictive firewall that is only allowing traffic from trusted clients.

## Deployment using Docker
### Setup steps
To start this as a docker container you need to have a system that is running docker.

First create a credentials.json file outside of the docker container (somewhere on your host system).
Just take a copy of the [sample file](/server/credentials.sample.json) and replace the credentials with real ones.

Then we simply start the docker container and provide the credentials.json we've created earlier.
Keep in mind that the `${PWD}` is the current directory. You might want to change this to a relative path on the host system.

#### Docker command to run the proxy
Map the config outside of the container (`-v`)

```bash
docker run -it --rm \
-v ${PWD}/credentials.json:/opt/app/credentials.json \
-p127.0.0.1:8080:8080 r6s-api-relay
```

**NOTE:** He've used the local-loopback interface for the port binding only. This means that even if you add firewall rules,
you can't reach this application from the outside.

## Disclaimer
This tool is in no way, shape or form affiliated with Ubisoft and its partners. 

This project is not intended for huge-scale data collection. Keep in mind that you're responsible for the API requests that are fired against the infrastructure of Ubisoft.

Try to build a cache in-between requests for the same user so that the API won't get flooded.
Keep the requests as-low-as-possible. We don't want Ubisoft to close this amazing posibillity (which they could do easly).



## Licsense
The project is licensed unter the GPLv3 see [LICENSE](LICENSE)
