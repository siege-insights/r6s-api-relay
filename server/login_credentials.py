from dataclasses import dataclass
from r6sapi import Auth


@dataclass
class LoginCredentials:
    username: str
    password: str

    session: Auth = None

    def is_logged_in(self):
        return self.session is not None
