import logging
import sys

import r6sapi
import r6sapi as api
from aiohttp import web
from r6sapi import Player

import player_details_response
import player_search_result
from login_provider import LoginProvider

root = logging.getLogger()
root.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)


class Server:
    login_provider: LoginProvider = LoginProvider()

    async def login(self) -> api.Auth:
        cred = self.login_provider.get_next_credentials()
        if not cred.is_logged_in():
            cred.session = api.Auth(cred.username, cred.password)
        return cred.session

    async def search_player(self, searchText):
        session = await self.login()
        try:
            p = await session.get_player(name=searchText, platform=api.Platforms.UPLAY)
            result_entry = player_search_result.PlayerSearchResult(id=p.id,
                                                                   user_id=p.userid,
                                                                   name=p.name)
        except r6sapi.exceptions.InvalidRequest as e:
            raise api.aiohttp.web.HTTPNotFound()

        return result_entry

    async def get_full_player(self, uuid) -> api.Player:
        session = await self.login()
        player: api.Player = await session.get_player(name=None, platform=api.Platforms.UPLAY, uid=uuid)
        await player.check_queues()
        await player.check_general()
        await player.check_level()
        await player.check_weapons()
        return player

    async def get_player_details(self, player_uuid, region) -> player_details_response:
        if region != api.RankedRegions.EU \
                and not region != api.RankedRegions.NA \
                and not api.RankedRegions.ASIA:
            logging.error("Unknown region is set: " + str(region))
            raise Exception("Unknown region: " + str(region))

        player: Player = await self.get_full_player(player_uuid)
        player.distance_travelled
        ranked: api.GameQueue = player.ranked
        rank: api.Rank = await player.get_rank(region=api.RankedRegions.EU)
        response = player_details_response.PlayerDetailsResponse(uuid=player.id,
                                                                 nickname=player.name,
                                                                 level=player.level,
                                                                 time_played=player.time_played,
                                                                 kills=player.kills,
                                                                 deaths=player.deaths,
                                                                 kill_assists=player.kill_assists,
                                                                 penetration_kills=player.penetration_kills,
                                                                 melee_kills=player.melee_kills,
                                                                 blind_kills=player.blind_kills,
                                                                 revives=player.revives,
                                                                 revives_denied=player.revives_denied,
                                                                 matches_won=player.matches_won,
                                                                 matches_lost=player.matches_lost,
                                                                 matches_played=player.matches_played,
                                                                 bullets_fired=player.bullets_fired,
                                                                 bullets_hit=player.bullets_hit,
                                                                 headshots=player.headshots,
                                                                 dbnos=player.dbnos,
                                                                 dbno_assists=player.dbno_assists,
                                                                 barricades_deployed=player.barricades_deployed,
                                                                 suicides=player.suicides,
                                                                 gadgets_destroyed=player.gadgets_destroyed,
                                                                 distance_traveled=player.distance_travelled,
                                                                 rank=rank,
                                                                 ranked=ranked,
                                                                 weapons=player.weapons)
        logging.debug("Response: " + str(response))
        return response

    def close_all_sessions(self):
        self.login_provider.close_sessions()


def serialize(obj):
    return obj.__dict__


routes = web.RouteTableDef()

ser = Server()


@routes.get('/user/{region}/{uuid}')
async def request_info(request):
    peername = request.transport.get_extra_info('peername')
    host, port = None, None
    if peername is not None:
        host, port = peername
    logging.info("API-Proxy-Request from " + str(host) + ":" + str(port))
    region = request.match_info.get('region')
    uuid = request.match_info.get('uuid')
    response = await ser.get_player_details(uuid, region)
    s_response = api.json.dumps(response, default=serialize)
    return web.json_response(text=s_response)


@routes.get('/search/{searchText}')
async def search(request):
    search_text = request.match_info.get('searchText')
    results = await ser.search_player(search_text)
    s_response = api.json.dumps(results, default=serialize)
    return web.json_response(text=s_response)


ser.login_provider.initialize()
app = web.Application()
app.router.add_routes(routes)
web.run_app(app)
ser.close_all_sessions()
print("Shutdown complete!")
