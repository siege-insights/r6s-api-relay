import json
import logging as log

from login_credentials import LoginCredentials


class LoginProvider:
    login_credentials = []
    current_index: int = 0

    def initialize(self):
        log.info("Initializing credentials ...")
        with open("credentials.json", "r") as file:
            data = json.load(file)
            for cred in data['credentials']:
                username = cred['username']
                password = cred['password']
                log.info("Adding user '" + username + "' ...")
                self.login_credentials.append(LoginCredentials(username=username, password=password))

    def get_next_credentials(self) -> LoginCredentials:
        log.debug("Requested next credential - using index " + str(self.current_index) + ' (' + self.login_credentials[self.current_index].username + ')')
        cred: LoginCredentials = self.login_credentials[self.current_index]
        if self.current_index >= len(self.login_credentials) - 1:
            self.current_index = 0
        else:
            self.current_index += 1
        return cred

    def close_sessions(self):
        for cred in self.login_credentials:
            cred.session.close()
