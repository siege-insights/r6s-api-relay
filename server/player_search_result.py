from dataclasses import dataclass


@dataclass
class PlayerSearchResult:
    id: str
    user_id: str
    name: str
