from dataclasses import dataclass

from r6sapi import GameQueue, Rank, Player, Weapon


@dataclass
class PlayerDetailsResponse:
    uuid: str
    nickname: str
    level: int
    time_played: int
    kills: int
    deaths: int
    kill_assists: int
    penetration_kills: int
    melee_kills: int
    blind_kills: int
    revives: int
    revives_denied: int
    matches_won: int
    matches_lost: int
    matches_played: int
    bullets_fired: int
    bullets_hit: int
    headshots: int
    dbnos: int
    dbno_assists: int
    barricades_deployed: int
    suicides: int
    gadgets_destroyed: int
    distance_traveled: int
    ranked: GameQueue
    rank: Rank
    weapons: Weapon
