# Make a executable environment for this python application to let it run as a server
FROM python:3.8-slim
MAINTAINER RAYs3T

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN pip3 install https://gitlab.com/RAYs3T/RainbowSixSiege-Python-API/-/archive/master/RainbowSixSiege-Python-API-master.tar.gz


RUN useradd -s /bin/bash r6s-api-relay
RUN mkdir /opt/app && chown -R r6s-api-relay /opt/app && chmod -R 750 /opt/app

USER r6s-api-relay
WORKDIR /opt/app

COPY server/*.py /opt/app/
ENTRYPOINT ["python3", "/opt/app/server.py"]
